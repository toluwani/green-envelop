<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', function () {
//     return view('layouts.master');
// });


// Route::get('/m', 'authcontroller@index' )->name('master')->middleware('auth');




//signup routes
Route:: get('/r','authcontroller@getsignup');
Route:: post('/r','authcontroller@postsignup');


//signin routes

Route:: get('/','authcontroller@getsigin')->name('home');
Route:: post('/l','authcontroller@postsigin');



Route::get('/logout','authcontroller@destroy')->middleware('auth');


// dashboard controller


Route::get('/a', 'dashcontroller@index' )->name('dash')->middleware('auth');

Route::post('/buy', 'dashcontroller@store' )->name('store');

Route::get('/v', 'dashcontroller@voucher' )->name('voucher')->middleware('auth');
Route::get('/g', 'dashcontroller@green' )->name('green')->middleware('auth');
Route::get('/s', 'dashcontroller@share' )->name('share')->middleware('auth');



// payment
Route::get('/buy', 'paymentcontroller@buy' )->name('buy')->middleware('auth');
Route::get('/c', 'paymentcontroller@checkout' )->name('checkout')->middleware('auth');
Route::post('/s', 'paymentcontroller@postshare' )->name('share')->middleware('auth');
