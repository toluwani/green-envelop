

<body>
    <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <header class="c-navbar">
        <a class="c-navbar__brand" href="#!">
            <img src="img/green.png" alt="Dashboard UI Kit">
        </a>
                   
       <!-- Navigation items that will be collapes and toggle in small viewports -->



<nav class="c-nav collapse" id="main-nav">
                <ul class="c-nav__list">
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="{{'/a'}}">Account</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="{{'/v'}}">My Vouchers</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="{{'/g'}}">Green Envelope</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="{{'/s'}}">Share Vouchers</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="">Sub Accounts</a>
                    </li>
                    
                    {{-- @if (Auth::check())   --}}
                    <li class="c-nav__item">
                            <a class="c-nav__link" href="{{'/logout'}}">logout</a>
                        </li>
                    {{-- @endif --}}
                    
                </ul>
            </nav>
            <!-- // Navigation items  -->
            

            <div class="c-dropdown u-ml-auto dropdown">
                <a  class="c-avatar c-avatar--xsmall has-dropdown dropdown-toggle" href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="c-avatar__img" src="img/avatar-72.jpg" alt="User's Profile Picture">
                </a>

                <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Sub Accounts</a>
                    
                </div>
            </div>

            <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button><!-- // .c-nav-toggle -->
        </header>

        <div class="c-toolbar u-mb-medium">
            <h3 class="c-toolbar__title has-divider"> welcome back </h3>
            <h5 class="c-toolbar__meta u-mr-auto">  {{ auth()->user()->fname}}</h5>

            
            
          
            {{-- <button type="" class="c-btn c-btn--fancy u-ml-small"  href="{{'/buy'}}">
              Buy vouchers
            </button> --}}


         
                <a class="c-btn c-btn--fancy u-ml-small " href="{{'/buy'}}"> Buy Vouchers</a>
          

                    </div><!-- // .c-toolbar -->