@include ('layouts.header')



<body class="o-page o-page--center">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <div class="o-page__card">
            <div class="c-card u-mb-xsmall">
                <header class="c-card__header u-pt-large">
                    <a class="c-card__icon" href="#!">
                        <img src="img/logo-login.svg" alt="Dashboard UI Kit">
                    </a>
                    <h1 class="u-h3 u-text-center u-mb-zero">Register to Get Started</h1>
                </header>
                
                <form class="c-card__body" action="{{'/r'}}" method="POST">
                    {{ csrf_field() }}
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input1">Fullname</label> 
                        <input class="c-input" type="text" id="input1" placeholder="Enter Full name" name="fname"> 
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input2">Email</label> 
                        <input class="c-input" type="email" id="input2" placeholder="Numbers, Letters..." name="email"> 
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input3"> state</label> 
                        <input class="c-input" type="text" id="input3" placeholder="state of origin" name="state"> 
                    </div>

                    <div class="c-field u-mb-small">
                            <label class="c-field__label" for="input3">address</label> 
                            <input class="c-input" type="text" id="input3" placeholder="address" name=" address"> 
                        </div>


                        
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input3">Phone number</label> 
                        <input class="c-input" type="text" id="input3" placeholder="Enter phone no" name="phone"> 
                    </div>

                        <div class="c-field u-mb-small">
                                <label class="c-field__label" for="input3"> Password</label> 
                                <input class="c-input" type="text" id="input3" placeholder=" Password" name="password"> 
                            </div>


                            
    
                                <input class="c-input" type="hidden"  value=""  name="envelop_id"> 
                     


                    <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Sign Up</button>


                    
                </form>
            </div>

            <div class="o-line">
                <a class="u-text-mute u-text-small" href="{{'/l'}}" title="Login">
                    <i class="fa fa-long-arrow-left u-mr-xsmall"></i> Already have an account, login instead
                </a>
            </div>
        
        </div>

        <script src="js/main.min.js"></script>
    </body>
</html>