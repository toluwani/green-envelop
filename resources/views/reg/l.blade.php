
@include ('layouts.header')



    <body class="o-page o-page--center">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <div class="o-page__card">
            <div class="c-card u-mb-xsmall">
                <header class="c-card__header u-pt-large">
                    <a class="c-card__icon" href="#!">
                        <img src="img/green.png" alt="Dashboard UI Kit">
                    </a>
                    <h1 class="u-h3 u-text-center u-mb-zero"> login.</h1>
                </header>


                <form class="c-card__body" action="{{'/l'}}" method="POST">
                    {{csrf_field()}}
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input1">Log in with your e-mail address</label> 
                        <input class="c-input" type="email" id="input1" placeholder="clark@dashboard.com" name="email"
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input2">Password</label> 
                        <input class="c-input" type="password" id="input2" placeholder="Numbers, Letters..." name="password"> 
                    </div>

                    <button class="c-btn c-btn--success " type="submit">login</button>


                    
                </form>
            </div>

            <div class="o-line">
                <a class="u-text-mute u-text-small" href="{{'/r'}}">
                    Don’t have an account yet? Get Started</a>


            </div>
        </div>

        <script src="js/main.min.js"></script>
    </body>
</html>