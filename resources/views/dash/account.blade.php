@extends('layouts.master')


@section ('content')




<div class="container">
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="c-state">
                <h3 class="c-state__title"> Envelop id</h3>
                <h4 class="c-state__number">{{ auth()->user()->envelop_id}}</h4>
                <p class="c-state__status">13% Increase</p>
                <span class="c-state__indicator">
                    <i class="fa fa-arrow-circle-o-down"></i>
                </span>
            </div><!-- // c-state -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="c-state c-state--success">
                <h3 class="c-state__title">Voucher Balance</h3>
                <h4 class="c-state__number"> N{{ auth()->user()->envelop_amount}}</h4>
                <p class="c-state__status">4% Increase</p>
                <span class="c-state__indicator">
                    <i class="fa fa-arrow-circle-o-up"></i>
                </span>
            </div><!-- // c-state -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="c-state c-state--warning">
                <h3 class="c-state__title">Envelop balance</h3>
                <h4 class="c-state__number">N{{ auth()->user()->envelop_amount}}</h4>
                <p class="c-state__status">21% Decrease</p>
                <span class="c-state__indicator">
                    <i class="fa fa-arrow-circle-o-down"></i>
                </span>
            </div><!-- // c-state -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="c-state c-state--danger">
                <h3 class="c-state__title">voucher avaliable</h3>
                <h4 class="c-state__number">3</h4>
                <p class="c-state__status">13% Decrease</p>
                <span class="c-state__indicator">
                    <i class="fa fa-arrow-circle-o-up"></i>
                </span>
            </div><!-- // c-state -->
        </div>
    </div><!-- // .row -->

    <div class="row u-mb-large">
        <div class="col-sm-12">
            <div class="c-table-responsive@desktop">
            <table class="c-table">

                <caption class="c-table__title">
                    Account Page<small>History table</small>
                    
                    <a class="c-table__title-action" href="#!">
                        <i class="fa fa-cloud-download"></i>
                    </a>
                </caption>

                <thead class="c-table__head c-table__head--slim">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head"> Envelop_id</th>
                      <th class="c-table__cell c-table__cell--head">Title</th>
                      <th class="c-table__cell c-table__cell--head">Description</th>
                      <th class="c-table__cell c-table__cell--head">Date</th>
                      <th class="c-table__cell c-table__cell--head">Trx_id</th>
                      <th class="c-table__cell c-table__cell--head">
                          <span class="u-hidden-visually">Actions</span>
                      </th>
                    </tr>
                </thead>

                <tbody>

                        @foreach ($users as $u)
                    <tr class ="c-table__row c-table__row--danger">
                        <td class="c-table__cell">{{$u->envelop_id}}
                            <small class="u-block u-text-mute">Google</small>
                        </td>

                        <td class="c-table__cell">{{$u->fname}}
                            <small class="u-block u-text-danger">Overdue</small>
                        </td>

                        

                        <td class="c-table__cell">{{$u->fname}}
                                <small class="u-block u-text-danger">Overdue</small>
                            </td>


                        <td class="c-table__cell">{{$u->created_at }}
                            <small class="u-block u-text-mute">Paid</small>
                        </td>

                        <td class="c-table__cell">
                            <i class="fa fa-circle-o u-color-info u-mr-xsmall"></i>Early Stages
                        </td>

                        <td class="c-table__cell u-text-right">
                            <div class="c-dropdown dropdown">
                                <button class="c-btn c-btn--secondary has-dropdown dropdown-toggle" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                
                                <div class="c-dropdown__menu dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <a class="c-dropdown__item dropdown-item" href="#">Reedem</a>
                                    <a class="c-dropdown__item dropdown-item" href="#">Share</a>

                                        
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    
                    
                    
                   
                    

                    
                </tbody>
            </table>
            </div>

            <nav class="c-pagination u-mt-small u-justify-between">
                <a class="c-pagination__control" href="#">
                    <i class="fa fa-caret-left"></i>
                </a>

                <p class="c-pagination__counter">Page 2 of 3</p>

                <a class="c-pagination__control" href="#">
                    <i class="fa fa-caret-right"></i>
                </a>
            </nav>
        </div>
    </div><!-- // .row -->
</div><!-- // .container -->













@endsection