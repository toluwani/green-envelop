@extends('layouts.master')


@section ('content')








<br> <br> 


<div class="container">
        <h3>  Got a voucher <br> want to share? </h3>

<div class="row">
        <div class="col-sm-6 col-md-6 u-mb-small">
            <div class="c-field">
                <label class="c-field__label u-hidden-visually" for="input1">Label</label>
                <input class="c-input" id="input1" type="text" placeholder="Enter voucher code ">
            </div>
        </div>


        <div class="col-sm-4 u-mb-medium">
                <!-- Button trigger modal -->
                <button type="button" class="c-btn c-btn--info" data-toggle="modal" data-target="#modal9">
                  share
                </button>

                <form class="c-card__body" action="{{'/s'}}" method="POST">
                    {{csrf_field()}}

                <!-- Modal -->
                <div class="c-modal c-modal--large modal fade" id="modal9" tabindex="-1" role="dialog" aria-labelledby="modal9" data-backdrop="static">
                    <div class="c-modal__dialog modal-dialog" role="document">
                        <div class="c-modal__content">
                            <div class="o-media c-card u-border-zero">
                                <!-- `font-size: 0` is a quick fix to remove weird spacing -->
                                <div class="o-media__img u-hidden-down@tablet" style="font-size: 0;">
                                    <img src="img/billing.jpg" alt="Image">
                                </div>

                                <div class="o-media__body u-p-medium">
                                    <div class="o-line u-align-items-start">
                                        <h3 class="u-mb-medium"> Enter details to share</h3>

                                        <span class="c-modal__close u-text-mute" data-dismiss="modal" aria-label="Close">
                                            <i class="u-opacity-medium fa fa-close"></i>
                                        </span>
                                    </div>
                                   

                                    <div class="c-field u-mb-xsmall">
                                        <div class="c-field has-icon-left">
                                            <span class="c-field__icon">
                                                <i class="fa fa-user-o"></i>
                                            </span> 
                                            <label class="c-field__label u-hidden-visually" for="input21">Email</label>
                                            <input class="c-input" id="input21" type="email" placeholder="Email" name="email">
                                        </div>
                                    </div>

                                    <div class="c-field u-mb-xsmall">
                                        <div class="c-field has-icon-left">
                                            <span class="c-field__icon">
                                                <i class="fa fa-credit-card"></i>
                                            </span> 
                                            <label class="c-field__label u-hidden-visually" for="input22">Card Number</label>
                                            <input class="c-input" id="input22" type="text" placeholder=" envelop_id" name="envelop_id">
                                        </div>
                                    </div>

                                   
                                    
                                    
                                   



                                    <div class="c-field u-mb-xsmall">
                                            <div class="c-field has-icon-left">
                                                <span class="c-field__icon">
                                                    <i class="fa fa-user-o"></i>
                                                </span> 
                                                <label class="c-field__label u-hidden-visually" for="input21">Email</label>
                                                <input class="c-input" id="input21" type="text" placeholder="phone number"  name="phone">
                                            </div>
                                        </div>

                                        <div class="c-field u-mb-xsmall">
                                            <div class="c-field has-icon-left">
                                                <span class="c-field__icon">
                                                    <i class="fa fa-user-o"></i>
                                                </span> 
                                                <label class="c-field__label u-hidden-visually" for="input21">Email</label>
                                                <input class="c-input" id="input21" type="text" placeholder="amount"  name="amount">
                                            </div>
                                        </div>
    


                                        <span class="c-divider has-text u-mb-small">
                                                and
                                            </span>

                                            
                                    <div class="c-field u-mb-xsmall">
                                            <div class="c-field has-icon-left">
                                                <span class="c-field__icon">
                                                    <i class="fa fa-user-o"></i>
                                                </span> 
                                                <label class="c-field__label u-hidden-visually" for="input21">Email</label>
                                                <input class="c-input" id="input21" type="text" placeholder="voucher code"  name="voucher">
                                            </div>
                                        </div>

                                        <button class="c-btn c-btn--success " type="submit"> share</button>

                                    </form>
                                </div>
                            </div>
                        </div><!-- // .c-modal__content -->
                    </div><!-- // .c-modal__dialog -->
                </div><!-- // .c-modal -->
            </div>




    </div>























    <div class="row">
        <div class="col-lg-6">
            <div class="c-credit-card u-mb-large">
                <div class="c-credit-card__card">
                    <img class="c-credit-card__logo" src="img/logo-visa.png" alt="Visa Logo">
                    <h5 class="c-credit-card__number">**** **** **** 3528</h5>
                    <p class="c-credit-card__status">Valid Thru 11/18</p>
                </div>

                <div class="c-credit-card__user">
                    <h3 class="c-credit-card__user-title">Your Bank Account</h3>
                    <p class="c-credit-card__user-meta">
                        <span class="u-text-mute">User Number:</span> 582 458
                    </p>
                    <p class="c-credit-card__user-meta">
                        <span class="u-text-mute">User ID:</span> WX84579522CY
                    </p>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="c-card u-p-medium u-mb-medium">

                <div class="u-flex u-justify-between u-align-items-center u-mb-small">
                    <h4 class="u-h5 u-mb-zero u-text-bold">How to use Dashboard</h4>
                    <a class="u-text-small" href="#">Visit FAQ Page</a>
                </div>
                
                <ul>
                    <li class="u-mb-xsmall">
                        <a class="u-text-small u-text-dark" href="#">How can I connect my bank account?</a>
                    </li>

                    <li class="u-mb-xsmall">
                        <a class="u-text-small u-text-dark" href="#">Why Dashboard doesn’t show any data?</a>
                    </li>
                    <li>
                        <a class="u-text-small u-text-dark" href="#">If I change my avatar in one version will it appears in next version?</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- // .container -->













@endsection 