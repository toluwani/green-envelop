@extends('layouts.master')


@section ('content')



<div class="row u-mb-large">
    <div class="col-sm-12">
        <div class="c-table-responsive@desktop">
        <table class="c-table">

            <caption class="c-table__title">
               My Vouchers<small>Voucher Balance</small>
                
                <a class="c-table__title-action" href="#!">
                    <i class="fa fa-cloud-download"></i>
                </a>
            </caption>

            <thead class="c-table__head c-table__head--slim">
                <tr class="c-table__row">
                  <th class="c-table__cell c-table__cell--head"> name</th>
                  <th class="c-table__cell c-table__cell--head"> voucher code</th>
                  <th class="c-table__cell c-table__cell--head">Amount</th>
                  <th class="c-table__cell c-table__cell--head">Voucher Type</th>
                  <th class="c-table__cell c-table__cell--head">Status</th>
                  <th class="c-table__cell c-table__cell--head">Expiry-date</th>

                  <th class="c-table__cell c-table__cell--head">
                      <span class="u-hidden-visually">Actions</span>
                  </th>
                </tr>
            </thead>

            <tbody>

                    @foreach ($voucher as $v)

                <tr class="c-table__row c-table__row--danger">
                    <td class="c-table__cell">{{auth()->user()->fname}}
                        
                    </td>

                    <td class="c-table__cell">{{$v->vouchers_code}}
                    </td>

                    <td class="c-table__cell">{{$v->amount}}

                        </td>


                    <td class="c-table__cell"> @if($v->type==1) green voucher
                            @elseif ( $v->type==2)feast voucher
                            @elseif ( $v->type==3)easy voucher
                            @else ( $v->type==4)booster voucher
                            @endif



                    </td>

                    <td class="c-table__cell">
                        <i class="fa fa-circle-o u-color-info u-mr-xsmall"></i>{{$v->status}}
                    </td>
                    <td class="c-table__cell">
                        <i class="fa fa-circle-o u-color-info u-mr-xsmall"></i>{{$v->expired_date}}
                    </td>

                    <td class="c-table__cell u-text-right">
                        <div class="c-dropdown dropdown">
                            <button class="c-btn c-btn--secondary has-dropdown dropdown-toggle" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                            
                            <div class="c-dropdown__menu dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <a class="c-dropdown__item dropdown-item" href="#">Reedem</a>
                                <a class="c-dropdown__item dropdown-item" href="#">Share</a>
                                
                            </div>
                        </div>
                    </td>
                </tr>

                
                @endforeach
                
               
                

                
            </tbody>
        </table>
        </div>

        <nav class="c-pagination u-mt-small u-justify-between">
            <a class="c-pagination__control" href="#">
                <i class="fa fa-caret-left"></i>
            </a>

            <p class="c-pagination__counter">Page 2 of 3</p>

            <a class="c-pagination__control" href="#">
                <i class="fa fa-caret-right"></i>
            </a>
        </nav>
    </div>
</div><!-- // .row -->
</div><!-- // .container -->







@endsection 